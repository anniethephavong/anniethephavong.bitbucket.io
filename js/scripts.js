console.log ('JS Loaded');

document.querySelector('.hamburger').addEventListener('click', function(){

    document.querySelector('nav').classList.toggle('open');

} )

$('.carousel').slick({
    dots: true,
    prevArrow: '<div class="slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></div>',
    nextArrow: '<div class="slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></div>'
});


var modalparent = document.getElementsByClassName("modal_multi");
var modal_btn_multi = document.getElementsByClassName("myBtn_multi");
var span_close_multi = document.getElementsByClassName("close_multi");

function setDataIndex() {
    for (i = 0; i < modal_btn_multi.length; i++) {
         modal_btn_multi[i].setAttribute('data-index', i);
         modalparent[i].setAttribute('data-index', i);
         span_close_multi[i].setAttribute('data-index', i);
     }
}

for (i = 0; i < modal_btn_multi.length; i++) {

     modal_btn_multi[i].onclick = function() {
         var ElementIndex = this.getAttribute('data-index');
         modalparent[ElementIndex].style.display = "block";
     };


     span_close_multi[i].onclick = function() {

         var ElementIndex = this.getAttribute('data-index');
         modalparent[ElementIndex].style.display = "none";

     };

 }


 window.onload = function() {
    setDataIndex();
};

 window.onclick = function(event) {
     if (event.target === modalparent[event.target.getAttribute('data-index')]) {
         modalparent[event.target.getAttribute('data-index')].style.display = "none";
     }

 };

